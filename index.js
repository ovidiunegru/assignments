
const FIRST_NAME = "Aurelian-Ovidiu";
const LAST_NAME = "Negru";
const GRUPA = "1092";


function initCaching() {
    var cache = {};
 
    Ccache.getCache = function() {
            return this;
    }
 
    cache.pageAccessCounter = function(PageName = 'home') {
            if (Object.getOwnPropertyNames(cache).includes(PageName.toLowerCase()))
                cache[PageName.toLowerCase()]++;
            else
                cache[PageName.toLowerCase()] = 1;
    }
 
    return cache;
 }

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

